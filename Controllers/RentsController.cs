﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using CarRentModels;
using Lab5Defend.Models;

namespace Lab5Defend.Controllers
{
    public class RentsController : Controller
    {
        private readonly Context _context;

        private List<ClientInfo> FullNameClient()
        {
            List<ClientInfo> infos = new List<ClientInfo>();

            foreach (Client c in _context.Clients)
                infos.Add(new ClientInfo(c));

            return infos;
        }

        private List<CarInfo> FullModelCar()
        {
            List<CarInfo> infos = new List<CarInfo>();

            foreach (Car c in _context.Cars)
                infos.Add(new CarInfo(c));

            return infos;
        }

        public RentsController(Context context)
        {
            _context = context;
        }

        // GET: Rents
        public async Task<IActionResult> Index()
        {
            var context = _context.Rents.Include(r => r.Car).Include(r => r.Client);
            return View(await context.ToListAsync());
        }

        // GET: Rents/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rent = await _context.Rents
                .Include(r => r.Car)
                .Include(r => r.Client)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (rent == null)
            {
                return NotFound();
            }

            return View(rent);
        }

        // GET: Rents/Create
        public IActionResult Create()
        {
            ViewData["CarId"] = new SelectList(FullModelCar(), "Id", "Model");
            ViewData["ClientId"] = new SelectList(FullNameClient(), "Id", "Name");
            return View();
        }

        // POST: Rents/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,RentStart,RentEnd,ClientId,CarId")] Rent rent)
        {
            if (ModelState.IsValid)
            {
                // Определление итоговой стоимости.
                rent.Car = await _context.Cars.FindAsync(rent.CarId);
                rent.CalcCost();

                _context.Add(rent);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["CarId"] = new SelectList(FullModelCar(), "Id", "Model", rent.CarId);
            ViewData["ClientId"] = new SelectList(FullNameClient(), "Id", "Name", rent.ClientId);
            return View(rent);
        }

        // GET: Rents/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rent = await _context.Rents.FindAsync(id);
            if (rent == null)
            {
                return NotFound();
            }
            ViewData["CarId"] = new SelectList(FullModelCar(), "Id", "Model", rent.CarId);
            ViewData["ClientId"] = new SelectList(FullNameClient(), "Id", "Name", rent.ClientId);
            return View(rent);
        }

        // POST: Rents/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,RentStart,RentEnd,ClientId,CarId")] Rent rent)
        {
            if (id != rent.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    rent.Car = await _context.Cars.FindAsync(rent.CarId);
                    rent.CalcCost();

                    _context.Update(rent);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RentExists(rent.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["CarId"] = new SelectList(FullModelCar(), "Id", "Model", rent.CarId);
            ViewData["ClientId"] = new SelectList(FullNameClient(), "Id", "Name", rent.ClientId);
            return View(rent);
        }

        // GET: Rents/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rent = await _context.Rents
                .Include(r => r.Car)
                .Include(r => r.Client)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (rent == null)
            {
                return NotFound();
            }

            return View(rent);
        }

        // POST: Rents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var rent = await _context.Rents.FindAsync(id);
            _context.Rents.Remove(rent);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RentExists(int id)
        {
            return _context.Rents.Any(e => e.Id == id);
        }
    }
}
