﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarRentModels;

namespace Lab5Defend.Models
{
    public class ClientInfo
    {
        public int Id { get; set; }
        public string Name { get; set; } // Хранит полное имя клиента.
        
        public ClientInfo(Client client)
        {
            Id = client.Id;
            Name = Id + ": " + client.Surname + " " + client.Name + " " + client.Patronymic;
        }
    }
}
