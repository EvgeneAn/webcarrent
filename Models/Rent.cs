﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CarRentModels
{
    public class Rent
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Поле не должно быть пустым")]
        [DataType(DataType.DateTime), Display(Name = "Начало аренды")]
        [DisplayFormat(DataFormatString = "{0:dd MMMM yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime RentStart { get; set; }
        
        [Required(ErrorMessage = "Поле не должно быть пустым")]
        [DataType(DataType.DateTime), Display(Name = "Окончание аренды")]
        [DisplayFormat(DataFormatString = "{0:dd MMMM yyyy HH:mm}", ApplyFormatInEditMode = true)]
        public DateTime RentEnd { get; set; }

        [Display(Name = "Клиент")]
        public int ClientId { get; set; }
        [Display(Name = "Клиент")]
        public Client Client { get; set; }

        [Display(Name = "Автомобиль")]
        public int CarId { get; set; }
        [Display(Name = "Автомобиль")]
        public Car Car { get; set; }

        [Display(Name = "Итоговая стоимость")]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal TotalCost { get; set; }

        public void CalcCost()
        {
            var dif = this.RentEnd - this.RentStart;
            decimal cost = dif.Days * 24 + dif.Hours;
            if (dif.Minutes >= 30)
                cost += 1;
            this.TotalCost = cost * this.Car.Price_hour;
        }
    }
}
