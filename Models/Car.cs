﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace CarRentModels
{
    public class Car
    {
        public List<Rent> Rents { get; set; }

        public int Id { get; set; }

        [Required(ErrorMessage = "Поле не должно быть пустым")]
        [Display(Name = "Модель")]
        public string Model { get; set; }       // марка

        [Required(ErrorMessage = "Поле не должно быть пустым")]
        [Display(Name = "Объем двигателя")]
        [Range(0.1, 1000, ErrorMessage = "Наименьший объем равняется 0,1")]
        [Column(TypeName = "decimal(4, 2)")]
        public decimal EngineV { get; set; }     // объем двигателя

        [Required(ErrorMessage = "Поле не должно быть пустым")]
        [Display(Name = "Расход топлива")]
        [Range(1.0, 200, ErrorMessage = "Наименьший расход равняется 1,0")]
        [Column(TypeName = "decimal(4, 2)")]
        public decimal Consumption { get; set; } // расход топлива

        [Required(ErrorMessage = "Поле не должно быть пустым")]
        [Display(Name = "Трансмиссия")]
        public string Transmission { get; set; }

        [Required(ErrorMessage = "Поле не должно быть пустым")]
        [Display(Name = "Цена за час")]
        [Range(0, 10000000, ErrorMessage = "Наименьшая цена равняется 0")]
        [Column(TypeName = "decimal(10, 2)")]
        public decimal Price_hour { get; set; }

        [Required(ErrorMessage = "Поле не должно быть пустым")]
        [Display(Name = "Макс. скорость")]
        [Range(typeof(int), "0", "300", ErrorMessage = "Скорость может находится в пределах от 0 до 300")]
        public int Max_speed { get; set; }

        [Display(Name = "Кондиционер")]
        public bool Conditioner { get; set; }

        [Required(ErrorMessage = "Поле не должно быть пустым")]
        [Display(Name = "Цвет")]
        public string Color { get; set; }

        [Required(ErrorMessage = "Поле не должно быть пустым")]
        [Display(Name = "Кузов")]
        public string Body { get; set; }        // кузов

        public Car() { }
    }
}
