﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CarRentModels;

namespace Lab5Defend.Models
{
    public class CarInfo
    {
        public int Id { get; set; }
        public string Model { get; set; } // Хранит полное название автомобиля.

        public CarInfo(Car car)
        {
            Id = car.Id;
            Model = Id + ": " + car.Model;
        }
    }
}
