﻿using Microsoft.EntityFrameworkCore;

namespace CarRentModels
{
    public class Context : DbContext
    {
        public DbSet<Car> Cars { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<Rent> Rents { get; set; }

        public Context(DbContextOptions<Context> options)
            :base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Client>().HasAlternateKey(u => u.Passport);
        }
    }
}
