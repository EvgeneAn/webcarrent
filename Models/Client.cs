﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace CarRentModels
{
    public class Client
    {
        public List<Rent> Rents { get; set; }

        public int Id { get; set; }

        [Required(ErrorMessage = "Поле не должно быть пустым")]
        [Display(Name = "Имя")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Поле не должно быть пустым")]
        [Display(Name = "Фамилия")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "Поле не должно быть пустым")]
        [Display(Name = "Отчество")]
        public string Patronymic { get; set; }

        [Required(ErrorMessage = "Поле не должно быть пустым")]
        [Display(Name = "Возраст")]
        [Range(typeof(int), "18", "85")]
        public int Age { get; set; }

        [Display(Name = "Телефон")]
        public string Phone { get; set; }

        [Required(ErrorMessage = "Поле не должно быть пустым")]
        [Display(Name = "Паспорт (серия номер)")]
        [RegularExpression("[0-9]{4} [0-9]{6}")]
        public string Passport { get; set; }

        public Client() { }
    }
}
